using System.Collections.Immutable;
using System.Text.RegularExpressions;
using Irnet.Core.Preprocessing;

namespace Irnet.Core;

/** Analyzed document container. It has the full plaintext content of the document, as well as its term frequencies. */
public class Document
{
    public string Content { get; }
    public FreqCounter TermFreqs { get; } = new();

    /** Analyzes the specified `content` and creates a new document from it. */
    internal Document(string content, ITermPreprocessor preprocessor, IImmutableSet<string> stopwords)
    {
        Content = content;

        var words = Regex.Split(content, @"[^\w]+");
        foreach (var word in words)
        {
            if (string.IsNullOrWhiteSpace(word)) // skip blank words
                continue;

            // trim and convert word to lowercase, then increment its counters
            string ppw = preprocessor.PreprocessTerm(word.Trim().ToLower());
            
            // skip stop word
            if (stopwords.Contains(ppw))
                continue;
            
            TermFreqs.Increment(ppw);
        }
    }
}
