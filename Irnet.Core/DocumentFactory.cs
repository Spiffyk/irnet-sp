using System.Collections.Immutable;
using Irnet.Core.Preprocessing;

namespace Irnet.Core;

public class DocumentFactory
{
    private readonly ITermPreprocessor _preprocessor;
    private readonly IImmutableSet<string> _stopwords;

    public DocumentFactory(ITermPreprocessor preprocessor, ISet<string> stopwords)
    {
        _preprocessor = preprocessor;
        _stopwords = stopwords.ToImmutableHashSet();
    }

    public Document Create(string content) => new Document(content, _preprocessor, _stopwords);
}