namespace Irnet.Core;

/** A modified dictionary. All values for keys that are not yet present in this dictionary are virtually considered to
 * be zero. This represents a word vector of a document. */
public class FreqCounter : Dictionary<string, int>
{
    /** Increments the specified `key`'s value. If the `key` does not yet exist in this counter, it is added and set
     * to 1. */
    public void Increment(string key)
    {
        if (!TryAdd(key, 1))
            this[key] += 1;
    }

    /** Gets the frequency for the specified `key`. If the `key` exists in the counter, retrieves its current value;
     * if it does not yet exist, returns 0. */
    public int GetFreq(string key)
    {
        if (TryGetValue(key, out int v))
            return v;
        
        return 0;
    }

    /** Calculates the euclidean length of the vector represented by this counter. */
    public double EuclideanLength()
    {
        long sum = 0;
        foreach (var (_, value) in this)
            sum += value * value;
        return Math.Sqrt(sum);
    }

    /** Calculates the dot product of the vectors represented by the specified counters. */
    public static long Dot(FreqCounter a, FreqCounter b)
    {
        long dot = 0;
        foreach (var (key, aValue) in a)
        {
            if (b.TryGetValue(key, out var bValue))
                dot += aValue * bValue;
        }

        return dot;
    }

    /** Calculates the cosine between the vectors represented by the specified counters. */
    public static double Cosine(FreqCounter a, FreqCounter b)
    {
        return Dot(a, b) / (a.EuclideanLength() * b.EuclideanLength());
    }
}
