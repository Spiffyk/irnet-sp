using Irnet.Core.Preprocessing;
using Irnet.Core.Querying.Ast;
using Irnet.Core.Querying.Parsing;
using Lucene.Net.Search;
using BooleanQuery = Irnet.Core.Querying.BooleanQuery;

namespace Irnet.Core;

/** The main index logic. */
public static partial class Index
{
    /** Documents indexed by their ID. */
    private static readonly Dictionary<string, Document> Documents = new();

    /** Terms as keys, list of document IDs as values. */
    private static readonly Dictionary<string, HashSet<string>> TermIndex = new();

    /** IDF (Inverse Document Frequency) value cache. Calculated from `DocumentFreqs`. */
    private static Dictionary<string, double>? _idfCache;

    /** The total number of documents in the index. */
    public static int DocumentCount => Documents.Count;

    /** The total number of unique terms in the index. */
    public static int TermCount => TermIndex.Count;

    /** The term preprocessor used by this index. */
    private static readonly ITermPreprocessor TermPreprocessor = new CzechStemmerTermPreprocessor();

    /** The factory used by this index to create documents. */
    private static readonly DocumentFactory DocumentFactory = new(TermPreprocessor, Stopwords.Words);

    /** Invalidates the `IdfCache`, forcing it to recalculate the next time it is needed. */
    private static void InvalidateIdfCache()
    {
        _idfCache = null;
    }

    /** Makes sure the `IdfCache` is valid. If it is not, recalculates it. */
    private static void EnsureIdfCache()
    {
        if (_idfCache != null)
            return;

        _idfCache = new();
        foreach (var (word, docIds) in TermIndex)
        {
            if (docIds.Count == 0)
                continue;
            
            double idf = Math.Log10(Documents.Count / (double)docIds.Count);
            _idfCache[word] = idf;
        }
    }
    
    /** Adds a new document with the specified `id` and `content`, incrementing the relevant counters */
    public static void AddDocument(string id, string content)
    {
        RemoveDocument(id); // If the document exists in the index, we're replacing it.
        
        InvalidateIdfCache();
        
        var d = DocumentFactory.Create(content);
        Documents.Add(id, d);
        foreach (var term in d.TermFreqs.Keys)
        {
            HashSet<string> docIds;
            if (!TermIndex.ContainsKey(term))
            {
                docIds = new();
                TermIndex.Add(term, docIds);
            }
            else
                docIds = TermIndex[term];

            docIds.Add(id);
        }
    }

    /** Removes the document with the specified `id` if it exists in the index, and returns `true`. If the document
     * does not exist, returns `false`. */
    public static bool RemoveDocument(string id)
    {
        if (!Documents.ContainsKey(id))
            return false;

        InvalidateIdfCache();
        
        var d = Documents[id];
        foreach (var term in d.TermFreqs.Keys)
        {
            TermIndex[term].Remove(id);
            if (TermIndex[term].Count == 0)
                TermIndex.Remove(term);
        }
        Documents.Remove(id);

        return true;
    }

    /** Gets an indexed document based on its ID. */
    public static Document? GetById(string id) =>
        Documents.TryGetValue(id, out var doc) ? doc : null;

    /** Checks whether the index contains a document with the specified ID. */
    public static bool ContainsId(string id) =>
        Documents.ContainsKey(id);

    /** Creates a vector query for this index based on the specified contents. */
    public static Document CreateVectorQuery(string content) => DocumentFactory.Create(content);

    /** Finds documents that contain the same terms as the specified `query`. */
    public static IEnumerable<(string, Document)> FindDocumentsByVectorQuery(Document query)
    {
        var documentIds = new HashSet<string>();
        foreach (var term in query.TermFreqs.Keys)
            if (TermIndex.TryGetValue(term, out var ids))
                foreach (var id in ids)
                    documentIds.Add(id);
        
        var result = new List<(string, Document)>();
        foreach (var id in documentIds)
            if (Documents.TryGetValue(id, out var document))
                result.Add((id, document));
        
        return result;
    }

    /** Parses a boolean query for this index based on the specified contents. */
    public static BooleanQueryParserResult CreateBooleanQuery(string content) => 
        BooleanQueryParser.Parse(content, TermPreprocessor);

    /** Finds documents based on the specified boolean query. */
    public static IEnumerable<(string, Document)> FindDocumentsByBooleanQuery(BooleanQuery query)
    {
        var result = new List<(string, Document)>();
        var documentIds = Evaluate(query.Ast);
        foreach (var id in documentIds)
            if (Documents.TryGetValue(id, out var document))
                result.Add((id, document));

        return result;
    }

    private static HashSet<string> Evaluate(IAstNode node)
    {
        switch (node)
        {
            case AndNode andNode:
                return EvaluateAnd(andNode);
            case OrNode orNode:
                return EvaluateOr(orNode);
            case NotNode notNode:
                return EvaluateNot(notNode);
            case TermNode termNode:
                return EvaluateTerm(termNode);
            default:
                throw new NotSupportedException($"Unsupported node type {node.GetType().Name}");
        }
    }

    private static HashSet<string> EvaluateAnd(AndNode andNode)
    {
        var left = Evaluate(andNode.Left);
        var right = Evaluate(andNode.Right);

        HashSet<string> src;
        HashSet<string> dst;
        if (left.Count <= right.Count)
        {
            src = left;
            dst = right;
        }
        else
        {
            src = right;
            dst = left;
        }

        return src
            .Where(documentId => dst.Contains(documentId))
            .ToHashSet();
    }

    private static HashSet<string> EvaluateOr(OrNode orNode)
    {
        var left = Evaluate(orNode.Left);
        var right = Evaluate(orNode.Right);
        
        HashSet<string> src;
        HashSet<string> dst;
        if (left.Count <= right.Count)
        {
            src = left;
            dst = right;
        }
        else
        {
            src = right;
            dst = left;
        }

        foreach (var documentId in src)
            dst.Add(documentId);

        return dst;
    }

    private static HashSet<string> EvaluateNot(NotNode notNode)
    {
        var operand = Evaluate(notNode.Operand);
        var result = Documents.Keys.ToHashSet();
        foreach (var documentId in operand)
            result.Remove(documentId);
        return result;
    }

    private static HashSet<string> EvaluateTerm(TermNode termNode) =>
        TermIndex.TryGetValue(termNode.Term, out var result)
            ? result
            : new HashSet<string>();

    /** Clears the index. */
    public static void Clear()
    {
        Documents.Clear();
        TermIndex.Clear();
        InvalidateIdfCache();
    }

    /** Calculates the specified `doc` relevance to the specified `query` by TF-IDF. */
    public static double CalculateTfIdfRelevance(Document doc, Document query)
    {
        EnsureIdfCache();

        double qsqSum = 0.0;
        double dsqSum = 0.0;
        double dqSum = 0.0;

        var terms = new HashSet<string>();
        foreach (var term in query.TermFreqs.Keys)
            terms.Add(term);
        foreach (var term in doc.TermFreqs.Keys)
            terms.Add(term);
        
        foreach (var term in terms) 
        {
            var idf = _idfCache![term]; // IdfCache is never null, because EnsureIdfCache was called
            int qf = query.TermFreqs.GetFreq(term);
            double q = (qf == 0)
                ? 0
                : (1 + Math.Log10(qf)) * idf;

            int tf = doc.TermFreqs.GetFreq(term);
            double d = (tf == 0)
                ? 0
                : (1 + Math.Log10(tf)) * idf;

            qsqSum += q * q;
            dsqSum += d * d;
            dqSum += d * q;
        }

        double qSum = Math.Sqrt(qsqSum);
        double dSum = Math.Sqrt(dsqSum);

        return dqSum / (qSum * dSum);
    }

    /** Calculates the specified `doc` relevance to the specified `query` by cosine similarity. */
    public static double CalculateCosineSimilarity(Document doc, Document query) =>
        FreqCounter.Cosine(doc.TermFreqs, query.TermFreqs);
}
