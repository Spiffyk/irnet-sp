using Lucene.Net.Analysis.Cz;

namespace Irnet.Core.Preprocessing;

public class CzechStemmerTermPreprocessor : ITermPreprocessor
{
    private static readonly CzechStemmer Stemmer = new();
    private char[] _buffer = new char[64];
    
    public string PreprocessTerm(string term)
    {
        term = term.Trim().ToLower();
        if (term.Length > _buffer.Length)
        {
            var newLength = _buffer.Length * 2;
            while (newLength < term.Length)
                newLength *= 2;
            _buffer = new char[newLength];
        }
        
        term.CopyTo(0, _buffer, 0, term.Length);
        var stemmedLength = Stemmer.Stem(_buffer, term.Length);
        return string.Intern(new string(_buffer, 0, stemmedLength));
    }
}