namespace Irnet.Core.Preprocessing;

/// <summary>
/// A preprocessor that simply returns the provided term, interned using <see cref="string.Intern"/>.
/// </summary>
public class IdentityTermPreprocessor : ITermPreprocessor
{
    public string PreprocessTerm(string term) => string.Intern(term.Trim().ToLower());
}