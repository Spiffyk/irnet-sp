namespace Irnet.Core.Preprocessing;

/// <summary>
/// An interface for preprocessing terms of documents in <see cref="Index"/>.
/// </summary>
public interface ITermPreprocessor
{
    /// <summary>
    /// Preprocesses the specified term. The result is always interned using <see cref="string.Intern"/>. The provided
    /// term is automatically trimmed and converted to lowercase, otherwise the behaviour of this method is undefined.
    /// </summary>
    string PreprocessTerm(string term);
}