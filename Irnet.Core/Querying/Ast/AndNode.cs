namespace Irnet.Core.Querying.Ast;

public record AndNode(IAstNode Left, IAstNode Right) : IAstNode
{
    public void PrettyPrint(int level = 0)
    {
        Console.WriteLine("AND {");
        AstPrintUtil.Write(level + 1, "Left = ");
        Left.PrettyPrint(level + 1);
        AstPrintUtil.Write(level + 1, "Right = ");
        Right.PrettyPrint(level + 1);
        AstPrintUtil.WriteLine(level, "}");
    }
}