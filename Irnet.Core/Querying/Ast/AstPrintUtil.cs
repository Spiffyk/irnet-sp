using System.Text;

namespace Irnet.Core.Querying.Ast;

public static class AstPrintUtil
{
    private static string Create(int level, string s)
    {
        var sb = new StringBuilder();
        for (int i = 0; i < level; i++)
            sb.Append("  ");
        sb.Append(s);
        return sb.ToString();
    }

    public static void Write(int level, string s) => Console.Write(Create(level, s));
    public static void WriteLine(int level, string s) => Console.WriteLine(Create(level, s));
}