namespace Irnet.Core.Querying.Ast;

public interface IAstNode
{
    public void PrettyPrint(int level = 0);
}