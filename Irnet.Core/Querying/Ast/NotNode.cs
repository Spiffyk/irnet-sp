namespace Irnet.Core.Querying.Ast;

public record NotNode(IAstNode Operand) : IAstNode
{
    public void PrettyPrint(int level = 0)
    {
        Console.WriteLine("NOT {");
        AstPrintUtil.Write(level + 1, "Operand = ");
        Operand.PrettyPrint(level + 1);
        AstPrintUtil.WriteLine(level, "}");
    }
}