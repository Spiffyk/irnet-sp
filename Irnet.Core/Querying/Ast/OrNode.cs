namespace Irnet.Core.Querying.Ast;

public record OrNode(IAstNode Left, IAstNode Right) : IAstNode
{
    public void PrettyPrint(int level = 0)
    {
        Console.WriteLine("OR {");
        AstPrintUtil.Write(level + 1, "Left = ");
        Left.PrettyPrint(level + 1);
        AstPrintUtil.Write(level + 1, "Right = ");
        Right.PrettyPrint(level + 1);
        AstPrintUtil.WriteLine(level, "}");
    }
}