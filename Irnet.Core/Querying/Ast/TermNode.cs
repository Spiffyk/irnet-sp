namespace Irnet.Core.Querying.Ast;

public record TermNode(string Term) : IAstNode
{
    public void PrettyPrint(int level = 0)
    {
        Console.WriteLine($"Term {{ {Term} }}");
    }
}