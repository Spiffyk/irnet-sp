using Irnet.Core.Querying.Ast;

namespace Irnet.Core.Querying;

public record BooleanQuery(IAstNode Ast);