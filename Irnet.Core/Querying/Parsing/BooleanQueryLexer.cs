using System.Collections;
using System.Diagnostics;
using System.Text;
using Irnet.Core.Querying.Parsing.Tokens;

namespace Irnet.Core.Querying.Parsing;

public class BooleanQueryLexer : IEnumerator<IQueryToken>
{
    private static readonly AndToken And = new();
    private static readonly NotToken Not = new();
    private static readonly OrToken Or = new();
    private static readonly ParenCloseToken ParenClose = new();
    private static readonly ParenOpenToken ParenOpen = new();

    private static WordToken Word(string word) => new(word);

    private InvalidToken Invalid(string reason)
    {
        _valid = false;
        return new InvalidToken(reason);
    }

    private bool _valid;
    private readonly CharEnumerator _query;
    private IQueryToken? _currentToken;
    
    public IQueryToken Current => _currentToken!;

    public BooleanQueryLexer(string query)
    {
        _query = query.GetEnumerator();
        AdvanceChar();
    }

    private bool AdvanceChar()
    {
        _valid = _query.MoveNext();
        return _valid;
    }

    private void MoveQuotedWord()
    {
        if (!AdvanceChar())
        {
            _currentToken = Invalid("Unbalanced quotes");
            return;
        }

        var sb = new StringBuilder();
        do
        {
            var c = _query.Current;

            if (char.IsWhiteSpace(c))
            {
                _currentToken = Invalid("Phrases are not supported");
                return;
            }

            if (c == '"')
            {
                _currentToken = Word(sb.ToString());
                AdvanceChar();
                return;
            }

            if (!char.IsLetterOrDigit(c))
            {
                _currentToken = Invalid("Only digits or letters are allowed in query terms.");
                return;
            }

            sb.Append(c);
        } while (AdvanceChar());

        _currentToken = Invalid("Unbalanced quotes");
    }

    private void MoveUnquotedWord()
    {
        var sb = new StringBuilder();
        do
        {
            char c = _query.Current;

            if (!char.IsLetterOrDigit(c))
                break;

            sb.Append(c);
        } while (AdvanceChar());

        Debug.Assert(sb.Length > 0);
        var word = sb.ToString();
        _currentToken = word switch
        {
            "AND" => And,
            "OR" => Or,
            "NOT" => Not,
            _ => Word(word)
        };
    }

    public bool MoveNext()
    {
        if (!_valid)
            return false;

        // Skip all leading whitespace
        char c;
        do
            c = _query.Current;
        while (char.IsWhiteSpace(c) && AdvanceChar());

        if (!_valid)
            return false;

        if (char.IsLetterOrDigit(c))
        {
            MoveUnquotedWord();
            return true;
        }

        switch (c)
        {
            case '(':
                _currentToken = ParenOpen;
                AdvanceChar();
                break;
            case ')':
                _currentToken = ParenClose;
                AdvanceChar();
                break;
            case '"':
                MoveQuotedWord();
                break;
            default:
                _currentToken = Invalid($"Invalid token {c}");
                break;
        }

        return true;
    }

    public void Reset()
    {
        _currentToken = null;
        _query.Reset();
        AdvanceChar();
    }

    object IEnumerator.Current => Current;

    public void Dispose()
    {
        _query.Dispose();
    }

}