using System.Diagnostics.CodeAnalysis;
using Irnet.Core.Preprocessing;
using Irnet.Core.Querying.Ast;
using Irnet.Core.Querying.Parsing.Tokens;

namespace Irnet.Core.Querying.Parsing;

/// <summary>
/// A recursive descent parser for queries.
/// </summary>
public static class BooleanQueryParser
{

    /// <summary>
    /// Parses the specified string representation of a query.
    /// </summary>
    public static BooleanQueryParserResult Parse(string query, ITermPreprocessor preprocessor)
    {
        var lexer = new BooleanQueryLexer(query);
        if (!lexer.MoveNext())
            return ParserFail("Empty query");

        var nodeResult = ParseExpression(lexer, preprocessor, 0, true);
        return nodeResult.Success 
            ? ParserSuccess(nodeResult.Node) 
            : ParserFail(nodeResult.Reason);
    }

    private static NodeResult ParseExpression(BooleanQueryLexer lexer,
                                              ITermPreprocessor preprocessor,
                                              int enclosed,
                                              bool start = false)
    {
        var result = ParseOrTypeExpression(lexer, preprocessor, enclosed, start);
        if (result.Continue && lexer.Current is ParenCloseToken && enclosed == 0)
            return Fail("Too many closing parentheses");

        return result;
    }

    private static NodeResult ParseTermExpression(BooleanQueryLexer lexer,
                                                  ITermPreprocessor preprocessor,
                                                  int enclosed,
                                                  bool start = false)
    {
        const string allowedTokens = "a word, a left parenthesis, or NOT";
        var token = lexer.Current;
        
        // NOT
        if (token is NotToken)
        {
            if (!lexer.MoveNext())
                return Fail("NOT must be followed by a term");
            var operandResult = ParseTermExpression(lexer, preprocessor, enclosed);
            if (!operandResult.Success)
                return operandResult;
            return Success(new NotNode(operandResult.Node), operandResult.Continue);
        }

        // Parentheses
        if (token is ParenOpenToken)
        {
            if (!lexer.MoveNext())
                return Fail("A parenthesis must be followed by an expression");
            var parenResult = ParseExpression(lexer, preprocessor, enclosed + 1);
            if (!parenResult.Success)
                return parenResult;
            if (!parenResult.Continue || lexer.Current is not ParenCloseToken)
                return Fail("Unbalanced parentheses");
            return Success(parenResult.Node, lexer.MoveNext());
        }

        // Word
        if (token is WordToken leftWordToken)
        {
            var ppw = preprocessor.PreprocessTerm(leftWordToken.Word);
            if (Stopwords.Words.Contains(ppw))
                return Fail($"The word '{ppw}' is a stop word - the result would be invalid.");
            return Success(new TermNode(ppw), lexer.MoveNext());
        }

        // FAIL
        return Fail(start
            ? $"A query must start with {allowedTokens}, but started with {token}."
            : $"Expected {allowedTokens}, but found {token}.");
    }
    
    private static NodeResult ParseOrTypeExpression(BooleanQueryLexer lexer,
                                                    ITermPreprocessor preprocessor,
                                                    int enclosed,
                                                    bool start = false)
    {
        var leftResult = ParseAndTypeExpression(lexer, preprocessor, enclosed, start);
        if (!leftResult.Success || !leftResult.Continue || lexer.Current is ParenCloseToken)
            return leftResult;

        var left = leftResult.Node!;
        var token = lexer.Current;

        if (token is OrToken)
        {
            if (!lexer.MoveNext())
                return Fail("Missing right operand for the OR operator");

            var rightResult = ParseOrTypeExpression(lexer, preprocessor, enclosed);
            if (!rightResult.Success)
                return rightResult;

            return Success(new OrNode(left, rightResult.Node), rightResult.Continue);
        }

        return Success(left);
    }

    private static NodeResult ParseAndTypeExpression(BooleanQueryLexer lexer, 
                                                     ITermPreprocessor preprocessor, 
                                                     int enclosed, 
                                                     bool start = false)
    {
        var leftResult = ParseTermExpression(lexer, preprocessor, enclosed, start);
        if (!leftResult.Success || !leftResult.Continue || lexer.Current is ParenCloseToken)
            return leftResult;

        var token = lexer.Current;

        if (token is OrToken)
            return leftResult;
        
        if (token is AndToken && !lexer.MoveNext())
            return Fail("Missing right operand for the AND operator.");
        
        var rightResult = ParseAndTypeExpression(lexer, preprocessor, enclosed);
        if (!rightResult.Success)
            return rightResult;

        return Success(new AndNode(leftResult.Node, rightResult.Node), rightResult.Continue);
    }

    private static BooleanQueryParserResult ParserSuccess(IAstNode ast) => 
        new(true, null, new BooleanQuery(ast));

    private static BooleanQueryParserResult ParserFail(string reason) => new(false, reason, null);

    private static NodeResult Success(IAstNode node, bool toContinue = true) => new NodeResult(true, null, node, toContinue);

    private static NodeResult Fail(string reason) => new NodeResult(false, reason, null, false);
    
    private record NodeResult
    {
        [MemberNotNullWhen(true, nameof(Node))]
        [MemberNotNullWhen(false, nameof(Reason))]
        // ReSharper disable once MemberHidesStaticFromOuterClass
        public bool Success { get; init; }
        public string? Reason { get; init; }
        public IAstNode? Node { get; init; }
        public bool Continue { get; init; }

        public NodeResult(bool success, string? reason, IAstNode? node, bool toContinue)
        {
            this.Success = success;
            this.Reason = reason;
            this.Node = node;
            this.Continue = toContinue;
        }
    }
}