using System.Diagnostics.CodeAnalysis;

namespace Irnet.Core.Querying.Parsing;

/// <summary>
/// Result of a parse method.
/// </summary>
public record BooleanQueryParserResult
{

    /// <summary>Whether the parse was successful.</summary>
    [MemberNotNullWhen(true, nameof(Query))]
    [MemberNotNullWhen(false, nameof(Reason))]
    public bool Success { get; init; }

    /// <summary>
    /// If the parser failed, this contains the description of the failure. If the query was parsed successfully,
    /// this parameter is <c>null</c>.
    /// </summary>
    public string? Reason { get; init; }

    /// <summary>
    /// If the query was parsed successfully, this is the result. If the parser failed, this is <c>null</c>.
    /// </summary>
    public BooleanQuery? Query { get; init; }

    /// <summary>
    /// Result of a parse method.
    /// </summary>
    /// <param name="success">Whether the parse was successful.</param>
    /// <param name="reason">
    /// If the parser failed, this contains the description of the failure. If the query was parsed successfully,
    /// this parameter is <c>null</c>.
    /// </param>
    /// <param name="query">
    /// If the query was parsed successfully, this is the result. If the parser failed, this is <c>null</c>.
    /// </param>
    public BooleanQueryParserResult(bool success, 
                                    string? reason, 
                                    BooleanQuery? query)
    {
        Success = success;
        Reason = reason;
        Query = query;
    }
}