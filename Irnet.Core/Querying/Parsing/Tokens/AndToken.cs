namespace Irnet.Core.Querying.Parsing.Tokens;

public record AndToken : IQueryToken;