namespace Irnet.Core.Querying.Parsing.Tokens;

public record InvalidToken(string Reason) : IQueryToken;