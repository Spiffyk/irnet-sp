namespace Irnet.Core.Querying.Parsing.Tokens;

public record NotToken : IQueryToken;