namespace Irnet.Core.Querying.Parsing.Tokens;

public record OrToken : IQueryToken;