namespace Irnet.Core.Querying.Parsing.Tokens;

public record ParenCloseToken : IQueryToken;