namespace Irnet.Core.Querying.Parsing.Tokens;

public record ParenOpenToken : IQueryToken;