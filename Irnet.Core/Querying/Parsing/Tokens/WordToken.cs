namespace Irnet.Core.Querying.Parsing.Tokens;

public record WordToken(string Word) : IQueryToken;