﻿using System.Diagnostics;

namespace Irnet.JavaImporter;

internal static class JavaRunner
{
    public const int ExitJavaUnavailable = int.MinValue;
    public const int ExitExecError = int.MinValue + 1;
    
    private static string? _pathToJavaExe;

    static JavaRunner()
    {
        var javaHome = Environment.GetEnvironmentVariable("JAVA_HOME");
        if (string.IsNullOrWhiteSpace(javaHome))
            return;

        var exeName = OperatingSystem.IsWindows() ? "java.exe" : "java";

        _pathToJavaExe = Path.Join(javaHome, "bin", exeName);
    }

    /// <summary>Checks whether Java is available to the program.</summary>
    public static bool IsAvailable() => _pathToJavaExe != null && File.Exists(_pathToJavaExe);

    public static Result Run(string jarFile, params string[] args)
    {
        if (_pathToJavaExe == null)
            return new Result(ExitJavaUnavailable);

        var info = new ProcessStartInfo(_pathToJavaExe);
        info.ArgumentList.Add("-jar");
        info.ArgumentList.Add(jarFile);
        info.RedirectStandardError = true;
        info.RedirectStandardOutput = true;
        foreach (var arg in args)
            info.ArgumentList.Add(arg);
        
        var process = Process.Start(info);
        if (process == null)
            return new Result(ExitExecError);
        var stdout = process.StandardOutput.ReadToEndAsync();
        var stderr = process.StandardError.ReadToEndAsync();
        process.WaitForExit();

        stdout.Wait();
        stderr.Wait();

        return new Result(process.ExitCode, stdout.Result, stderr.Result);
    }

    public record Result(int StatusCode, string StdOut = "", string StdErr = "")
    {
        public bool Success => StatusCode == 0;
    }

}