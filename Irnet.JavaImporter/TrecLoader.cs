using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Reflection;
using System.Text.Json.Nodes;
using Irnet.Parsers;

namespace Irnet.JavaImporter;

public static class TrecLoader
{

    private static string? _tempJar = null;

    public static bool IsAvailable(out string reason)
    {
        if (!File.Exists(GetJarFile()))
        {
            reason = "The Java loader module is not available.";
            return false;
        }
        
        if (!JavaRunner.IsAvailable())
        {
            reason = "Could not find an appropriate Java version. Is JAVA_HOME set properly?";
            return false;
        }

        reason = string.Empty;
        return true;
    }

    public static Result LoadFile(string file, DocumentConsumer documentConsumer)
    {
        var runResult = JavaRunner.Run(GetJarFile(), file);
        if (!runResult.Success)
            return new Result(
                success: false,
                reason: $"Importer exited with code {runResult.StatusCode}, stderr was:\n{runResult.StdErr}");

        try
        {
            var rootNode = JsonNode.Parse(runResult.StdOut);
            if (rootNode == null)
                return new Result(false, "JSON was null");

            try
            {
                var array = rootNode.AsArray();
                foreach (var docNode in array)
                {
                    if (docNode == null)
                        return new Result(false, "Document node was null");

                    var content = docNode["text"]?.ToString();
                    if (content == null)
                        return new Result(false, "Content must be provided");
                    var id = docNode["id"]?.ToString();
                    if (id == null)
                        return new Result(false, "ID must be provided");
                    
                    documentConsumer.Invoke(id, content);
                }
            }
            catch (Exception e)
            {
                return new Result(false, $"Expected root to be an array. {e.Message}");
            }
        }
        catch (Exception e)
        {
            return new Result(false, $"Failed to parse importer result: {e.Message}");
        }

        return new Result(true);
    }

    private static string GetJarFile()
    {
        if (_tempJar == null || !File.Exists(_tempJar))
        {
            _tempJar = Path.Join(Path.GetTempPath(), "irnet-importer.jar");
            var resourceStream = typeof(TrecLoader).Assembly.GetManifestResourceStream("importer.jar")
                ?? throw new InvalidDataException("Could not get resource stream for Java importer");

            var bytes = new byte[resourceStream.Length];
            int rl = resourceStream.Read(bytes, 0, bytes.Length);
            Debug.Assert(rl == bytes.Length);

            using var os = File.OpenWrite(_tempJar);
            os.Write(bytes, 0, bytes.Length);
        }
        return _tempJar;
    }

    public record Result
    {
        [MemberNotNullWhen(false, nameof(Reason))]
        public bool Success { get; init; }
        public string? Reason { get; init; }

        public Result(bool success, string? reason = null)
        {
            Success = success;
            Reason = reason;
        }
    }
    
}