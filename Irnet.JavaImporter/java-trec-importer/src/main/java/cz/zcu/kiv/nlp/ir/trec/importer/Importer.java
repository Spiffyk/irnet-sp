package cz.zcu.kiv.nlp.ir.trec.importer;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.zcu.kiv.nlp.ir.trec.SerializedDataHelper;
import cz.zcu.kiv.nlp.ir.trec.data.Document;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class Importer {
    public static final int EXIT_INVALID_ARGS = 1;
    public static final int EXIT_NO_FILE = 2;
    public static final int EXIT_IO = 3;

    public static void main(String[] args) {
        if (args.length < 1) {
            System.err.println("Required one argument: input file name");
            System.exit(EXIT_INVALID_ARGS);
        }

        File file = new File(args[0]);
        if (!file.exists()) {
            System.err.println("File '" + file.getAbsolutePath() + "' does not exist.");
            System.exit(EXIT_NO_FILE);
        }

        List<Document> documents = SerializedDataHelper.loadDocument(file);
        ObjectMapper mapper = new ObjectMapper();

        try {
            mapper.writer()
                    .writeValues(System.out)
                    .write(documents);
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(EXIT_IO);
        }
    }

}
