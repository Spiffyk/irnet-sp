using System.Text;

namespace Irnet.Parsers;

/** A static parser for 'legacy' crawl files. */
public static class CrawlFileParser
{
    /** A heading that indicates that a new document starts on this line. */
    private const String DocumentHeading = ">>>>>>> PAGE: ";

    /** Reads the specified `file` and passes all documents from it to the `consumer`. */
    public static int Parse(string file, DocumentConsumer consumer)
    {
        int count = 0;
        string? id = null;
        var sb = new StringBuilder();
        foreach (var rawLine in File.ReadLines(file))
        {
            if (string.IsNullOrWhiteSpace(rawLine))
                continue;
            var line = rawLine.Trim();

            if (line.StartsWith(DocumentHeading))
            {
                // Pass previous document to the indexer
                if (id != null)
                {
                    consumer.Invoke(id, sb.ToString());
                    count++;
                    sb.Clear();
                }
                
                // Start a new document
                id = line.Substring(line.IndexOf(' ', DocumentHeading.Length)).Trim();
            }
            else
            {
                // Skip lines before the first page heading
                if (id == null)
                    continue;

                // Add document line
                sb.AppendLine(line.Trim());
            }
        }
        
        // Pass the last document to the indexer
        if (id != null)
        {
            consumer.Invoke(id, sb.ToString());
            count++;
        }

        return count;
    }
}
