namespace Irnet.Parsers;

/** A consumer that takes a document's ID and content and does something with it. */
public delegate void DocumentConsumer(string id, string content);