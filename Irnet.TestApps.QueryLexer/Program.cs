﻿using Irnet.Core.Querying.Parsing;

Console.Write("Query: ");
var query = Console.ReadLine();
if (string.IsNullOrEmpty(query))
{
    Console.WriteLine("Empty query. Exiting.");
    return;
}

var lexer = new BooleanQueryLexer(query);
while (lexer.MoveNext())
    Console.WriteLine(lexer.Current);

Console.WriteLine("END OF STREAM");