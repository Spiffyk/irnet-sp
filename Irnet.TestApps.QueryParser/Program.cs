﻿using Irnet.Core;
using Irnet.Core.Preprocessing;
using Irnet.Core.Querying.Parsing;

Console.Write("Query: ");

var query = Console.ReadLine();
if (string.IsNullOrEmpty(query))
{
    Console.WriteLine("Empty query. Exiting.");
    return;
}

var preprocessor = new IdentityTermPreprocessor();
var parseResult = BooleanQueryParser.Parse(query, preprocessor);
if (!parseResult.Success)
{
    Console.WriteLine(parseResult);
    return;
}

parseResult.Query.Ast.PrettyPrint();