﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Irnet.Ui.Db.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "QueryHistoryEntries",
                columns: table => new
                {
                    Query = table.Column<string>(type: "TEXT", nullable: false),
                    Used = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QueryHistoryEntries", x => x.Query);
                });

            migrationBuilder.CreateIndex(
                name: "IXUsed",
                table: "QueryHistoryEntries",
                column: "Used");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "QueryHistoryEntries");
        }
    }
}
