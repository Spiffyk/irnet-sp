﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Irnet.Ui.Db.Migrations
{
    public partial class BooleanQueries : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "QueryHistoryEntries");

            migrationBuilder.CreateTable(
                name: "BooleanQueryHistoryEntries",
                columns: table => new
                {
                    Query = table.Column<string>(type: "TEXT", nullable: false),
                    Used = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BooleanQueryHistoryEntries", x => x.Query);
                });

            migrationBuilder.CreateTable(
                name: "VectorQueryHistoryEntries",
                columns: table => new
                {
                    Query = table.Column<string>(type: "TEXT", nullable: false),
                    Used = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VectorQueryHistoryEntries", x => x.Query);
                });

            migrationBuilder.CreateIndex(
                name: "IXBooleanUsed",
                table: "BooleanQueryHistoryEntries",
                column: "Used");

            migrationBuilder.CreateIndex(
                name: "IXVectorUsed",
                table: "VectorQueryHistoryEntries",
                column: "Used");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BooleanQueryHistoryEntries");

            migrationBuilder.DropTable(
                name: "VectorQueryHistoryEntries");

            migrationBuilder.CreateTable(
                name: "QueryHistoryEntries",
                columns: table => new
                {
                    Query = table.Column<string>(type: "TEXT", nullable: false),
                    Used = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QueryHistoryEntries", x => x.Query);
                });

            migrationBuilder.CreateIndex(
                name: "IXUsed",
                table: "QueryHistoryEntries",
                column: "Used");
        }
    }
}
