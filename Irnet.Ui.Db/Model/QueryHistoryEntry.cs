using System.ComponentModel.DataAnnotations;

namespace Irnet.Ui.Db;

internal abstract class QueryHistoryEntry
{
    [Key] public string Query { get; set; } = string.Empty;

    public DateTime Used { get; set; } = DateTime.Now;
}