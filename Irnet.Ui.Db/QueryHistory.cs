using System.Text.RegularExpressions;
using Microsoft.EntityFrameworkCore;

namespace Irnet.Ui.Db;

/// <summary>Query history operations.</summary>
public static class QueryHistory
{
    /// <summary>Gets `n` newest entries in the query history.</summary>
    public static IList<string> GetNewestVector(int n = 5)
    {
        using var ctx = new UiDbContext();
        return GetNewest(ctx.VectorQueryHistoryEntries, n);
    }

    /// <summary>Gets `n` newest entries in the query history.</summary>
    public static IList<string> GetNewestBoolean(int n = 5)
    {
        using var ctx = new UiDbContext();
        return GetNewest(ctx.BooleanQueryHistoryEntries, n);
    }

    /// <summary>Gets `n` newest entries in the query history.</summary>
    private static IList<string> GetNewest<T>(IQueryable<T> dbSet, int n) where T : QueryHistoryEntry
    {
        return dbSet
            .OrderByDescending(e => e.Used)
            .Select(e => e.Query)
            .Take(n)
            .ToList();
    }

    /// <summary>Adds the specified `query` into the history manager.</summary>
    public static void PutVector(string query)
    {
        using var ctx = new UiDbContext();
        var words = Regex.Split(query, @"\s+")
            .Select(t => t.ToLower())
            .ToArray();

        var nquery = string.Join(' ', words);
        Put(nquery, ctx, ctx.VectorQueryHistoryEntries, () => new());
    }

    /// <summary>Adds the specified `query` into the history manager.</summary>
    public static void PutBoolean(string query)
    {
        using var ctx = new UiDbContext();
        var words = Regex.Split(query, @"\s+").ToArray();

        var nquery = string.Join(' ', words);
        Put(nquery, ctx, ctx.BooleanQueryHistoryEntries, () => new());
    }

    /// <summary>
    /// Adds the specified `query` into the history manager. If the query already exists, it only gets updated so it can
    /// be retrieved as first when ordered by last usage.
    /// The query gets normalized, i.e. whitespace gets normalized, query is converted to lowercase etc.
    /// </summary>
    private static void Put<T>(string nquery,
                               DbContext ctx,
                               DbSet<T> dbSet,
                               Producer<T> entryGenerator)
        where T : QueryHistoryEntry
    {

        var entry = dbSet.FirstOrDefault(entry => entry.Query == nquery);
        if (entry == null)
        {
            entry = entryGenerator.Invoke();
            entry.Query = nquery;
            dbSet.Add(entry);
        }

        entry.Used = DateTime.Now;

        ctx.SaveChanges();
    }

    private delegate T Producer<out T>();
}