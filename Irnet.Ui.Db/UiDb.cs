using Microsoft.EntityFrameworkCore;

namespace Irnet.Ui.Db;

/** Main manager of UI-related persistence data. */
public static class UiDb
{
    /** Ensures that the persistence data exists. */
    public static void Touch()
    {
        using (var ctx = new UiDbContext())
        {
            ctx.Database.Migrate();
        }
    }
}