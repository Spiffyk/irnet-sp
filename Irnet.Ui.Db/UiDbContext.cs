﻿using System.Data.Common;
using Irnet.Utils;
using Microsoft.EntityFrameworkCore;

namespace Irnet.Ui.Db;

internal class UiDbContext : DbContext
{
    private const string DbFile = "irnet-ui.sqlite";
    
    public DbSet<VectorQueryHistoryEntry> VectorQueryHistoryEntries { get; set; }
    public DbSet<BooleanQueryHistoryEntry> BooleanQueryHistoryEntries { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        base.OnConfiguring(optionsBuilder);
        
        var csb = new DbConnectionStringBuilder();
        csb.Add("Data source", Path.Join(Config.GetDirectory(), DbFile));
        
        optionsBuilder.UseSqlite(csb.ConnectionString);
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);

        modelBuilder.Entity<VectorQueryHistoryEntry>()
            .HasIndex(entry => entry.Used, "IXVectorUsed");
        modelBuilder.Entity<BooleanQueryHistoryEntry>()
            .HasIndex(entry => entry.Used, "IXBooleanUsed");
    }
}