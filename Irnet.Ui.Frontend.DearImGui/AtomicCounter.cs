namespace Irnet.Ui.Frontend.DearImGui;

public class AtomicCounter
{
    private uint _counter;

    public uint Increment() => Interlocked.Increment(ref _counter);
    public uint Decrement() => Interlocked.Decrement(ref _counter);
    public bool HasTasks() => Volatile.Read(ref _counter) > 0;
}