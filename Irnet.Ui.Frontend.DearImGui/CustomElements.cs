using System.Numerics;
using ImGuiNET;
using Irnet.Utils;

namespace Irnet.Ui.Frontend.DearImGui;

public static class CustomElements
{
    private const float ListHeight = 16;
    private const float RootListWidth = 200;
    private const float FileListWidth = 500;
    
    private static Dictionary<string, FileDialogState> _states = new();

    private static int _selectedRoot = 0;
    private static List<string>? _roots = null;

    private static string[] _spinnerFrames =
    {
        "|>     |",
        "|>>    |",
        "|>>>   |",
        "| >>>  |",
        "|  >>> |",
        "|   >>>|",
        "|    >>|",
        "|     >|",
        "|      |",
        "|     <|",
        "|    <<|",
        "|   <<<|",
        "|  <<< |",
        "| <<<  |",
        "|<<<   |",
        "|<<    |",
        "|<     |",
        "|      |",
    };

    private static bool IsAccessible(this DirectoryInfo di)
    {
        try
        {
            di.GetDirectories();
            return true;
        }
        catch (Exception)
        {
            return false;
        }
    }

    private static bool IsUnixSystem(this DirectoryInfo di)
    {
        if (!OperatingSystem.IsLinux() && !OperatingSystem.IsMacOS())
            return false;

        string[] elems = di.FullName.Split(Path.DirectorySeparatorChar,
            StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);
        if (elems.Length <= 0)
            return false;

        switch (elems[0])
        {
            case "sys":
            case "proc":
            case "dev":
            case "run":
            case "tmp":
                return true;
        }

        return false;
    }
    
    private static void RootList(FileDialogState state, ref string output)
    {
        if (_roots == null)
        {
            _roots = new();
            _roots.AddRange(DriveInfo.GetDrives()
                .Where(di => di.IsReady)
                .Select(di => di.RootDirectory)
                .Where(di => !string.IsNullOrWhiteSpace(di.FullName))
                .Where(di => di.Attributes.HasFlag(FileAttributes.Directory))
                .Where(di => di.IsAccessible())
                .Where(di => !di.IsUnixSystem())
                .Select(di => di.FullName));
            _roots.Add(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile));
            _roots.Add(Config.GetDirectory());
            _roots.Sort();
        }

        bool picked = false;
        if (ImGui.BeginListBox("##Roots", new Vector2(RootListWidth, ListHeight * ImGui.GetTextLineHeightWithSpacing())))
        {
            for (int i = 0; i < _roots.Count; i++)
            {
                bool selected = i == _selectedRoot;
                if (ImGui.Selectable(_roots[i], selected, ImGuiSelectableFlags.AllowDoubleClick))
                {
                    _selectedRoot = i;
                    if (ImGui.IsMouseDoubleClicked(ImGuiMouseButton.Left))
                        picked = true;
                }
                
                if (selected)
                    ImGui.SetItemDefaultFocus();
            }

            ImGui.EndListBox();
        }

        if (picked && _selectedRoot >= 0 && _selectedRoot < _roots.Count)
            output = _roots[_selectedRoot];
    }
    
    private static bool FileList(FileDialogState state)
    {
        bool picked = false;
        if (ImGui.BeginChild("##File list parent", 
                new Vector2(FileListWidth, ListHeight * ImGui.GetTextLineHeightWithSpacing())))
        {
            ImGui.Text(state.CurrentDirectory);
            if (ImGui.BeginListBox("##File picker",
                    new Vector2(FileListWidth, (ListHeight - 1) * ImGui.GetTextLineHeightWithSpacing())))
            {
                for (int i = 0; i < state.FilesCache.Count; i++)
                {
                    bool selected = i == state.Selected;
                    if (ImGui.Selectable(state.FilesCache[i].Display, selected, ImGuiSelectableFlags.AllowDoubleClick))
                    {
                        state.Selected = i;
                        if (ImGui.IsMouseDoubleClicked(ImGuiMouseButton.Left))
                            picked = true;
                    }

                    if (selected)
                        ImGui.SetItemDefaultFocus();
                }

                ImGui.EndListBox();
            }
            ImGui.EndChild();
        }

        return picked;
    }

    public static void Spinner(ImGuiDriver drv)
    {
        ImGui.PushFont(drv.MonospaceFont);
        var index = (int) (ImGui.GetTime() * 18) % _spinnerFrames.Length;
        ImGui.Text(_spinnerFrames[index]);
        ImGui.PopFont();
    }

    public static bool FileDialog(string label, ref bool open, ref string output)
    {
        if (!open)
            return false;
        
        if (!ImGui.Begin(label, ref open, ImGuiWindowFlags.AlwaysAutoResize | ImGuiWindowFlags.NoResize))
            return false;
        
        try
        {
            if (!_states.TryGetValue(label, out var state))
            {
                state = new FileDialogState();
                _states.Add(label, state);
            }

            RootList(state, ref output);
            
            if (string.IsNullOrWhiteSpace(state.CurrentDirectory) || output != state.CurrentDirectory)
                output = state.SetDirectory(output);

            ImGui.SameLine();
            bool picked = FileList(state);

            if (ImGui.Button("Cancel"))
            {
                open = false;
                return false;
            }

            ImGui.SameLine();
            picked |= ImGui.Button("Open");

            if (picked && state.Selected >= 0 && state.Selected < state.FilesCache.Count)
            {
                output = Path.GetFullPath(Path.Join(state.CurrentDirectory, state.FilesCache[state.Selected].Name));
                if (Directory.Exists(output))
                    return false;

                if (File.Exists(output))
                    open = false;

                return true;
            }

            return false;
        }
        finally
        {
            ImGui.End();
        }
    }

    private class FileDialogState
    {
        public string CurrentDirectory { get; private set; } = string.Empty;
        public List<FileListing> FilesCache { get; } = new();
        public int Selected { get; set; } = -1;

        public string SetDirectory(string directory)
        {
            CurrentDirectory = directory;
            FilesCache.Clear();
            Selected = -1;

            if (!Directory.Exists(CurrentDirectory) && File.Exists(CurrentDirectory))
                CurrentDirectory = Directory.GetParent(CurrentDirectory)?.FullName ?? $"{Path.PathSeparator}";
            
            ReadDirectory();

            return CurrentDirectory;
        }

        public void ReadDirectory()
        {
            if (CurrentDirectory != Directory.GetDirectoryRoot(CurrentDirectory))
                FilesCache.Add(new FileListing("..", true));
            
            try
            {
                FilesCache.AddRange(Directory.GetFileSystemEntries(CurrentDirectory)
                    .Where(f => !new FileInfo(f).Attributes.HasFlag(FileAttributes.Hidden))
                    .Select(Path.GetFileName)
                    .Where(f => !string.IsNullOrWhiteSpace(f))
                    .Select(f => new FileListing(f!, Directory.Exists(Path.Join(CurrentDirectory, f!))))
                    .OrderBy(fl => !fl.IsDirectory)
                    .ThenBy(fl => fl.Name));
            }
            catch (DirectoryNotFoundException)
            {
                Console.WriteLine($"Directory '{CurrentDirectory}' not found; returning empty");
            }
        }
    }

    private record FileListing(string Name, bool IsDirectory)
    {
        private Lazy<string> _display = new(() =>
        {
            if (IsDirectory)
                return $"> {Name}";

            return Name;
        });

        public string Display => _display.Value;
    }
}
