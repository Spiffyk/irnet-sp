using System.Diagnostics;
using System.Numerics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using ImGuiNET;
using Irnet.Utils;
using Veldrid;
using Veldrid.Sdl2;

namespace Irnet.Ui.Frontend.DearImGui;

/** Manages graphics resources and input data for ImGui. */
public class ImGuiDriver : IDisposable
{
    private readonly GraphicsDevice _gd;

    private DeviceBuffer _vertexBuffer;
    private DeviceBuffer _indexBuffer;
    private readonly DeviceBuffer _projMatrixBuffer;
    private readonly Texture _fontTexture;
    private readonly TextureView _fontTextureView;
    private readonly Shader _vertexShader;
    private readonly Shader _fragmentShader;
    private readonly ResourceLayout _layout;
    private readonly ResourceLayout _textureLayout;
    private readonly Pipeline _pipeline;
    private readonly ResourceSet _mainResourceSet;
    private readonly ResourceSet _fontTextureResourceSet;

    private readonly IntPtr _fontAtlasId = (IntPtr) 1;

    private int _windowWidth;
    private int _windowHeight;
    private bool _frameBegun = false;
    private long _lastTicks;
    
    public ImFontPtr MainFont { get; set; }
    public ImFontPtr MonospaceFont { get; set; }
    
    private static readonly IReadOnlyDictionary<Key, ImGuiKey> ImGuiKeys = new Dictionary<Key, ImGuiKey>
    {
        [Key.BackSpace] = ImGuiKey.Backspace,
        [Key.Delete] = ImGuiKey.Delete,
        [Key.Up] = ImGuiKey.UpArrow,
        [Key.Right] = ImGuiKey.RightArrow,
        [Key.Down] = ImGuiKey.DownArrow,
        [Key.Left] = ImGuiKey.LeftArrow,
        [Key.Tab] = ImGuiKey.Tab,
        [Key.Enter] = ImGuiKey.Enter,
        
        [Key.KeypadEnter] = ImGuiKey.KeypadEnter,
        
        [Key.ShiftLeft] = ImGuiKey.LeftShift,
        [Key.ShiftRight] = ImGuiKey.RightShift,
        [Key.ControlLeft] = ImGuiKey.LeftCtrl,
        [Key.ControlRight] = ImGuiKey.RightCtrl,
        [Key.AltLeft] = ImGuiKey.LeftAlt,
        [Key.AltRight] = ImGuiKey.RightAlt,
        [Key.WinLeft] = ImGuiKey.LeftSuper,
        [Key.WinRight] = ImGuiKey.RightSuper,
        
        [Key.A] = ImGuiKey.A,
        [Key.B] = ImGuiKey.B,
        [Key.C] = ImGuiKey.C,
        [Key.D] = ImGuiKey.D,
        [Key.E] = ImGuiKey.E,
        [Key.F] = ImGuiKey.F,
        [Key.G] = ImGuiKey.G,
        [Key.H] = ImGuiKey.H,
        [Key.I] = ImGuiKey.I,
        [Key.J] = ImGuiKey.J,
        [Key.K] = ImGuiKey.K,
        [Key.L] = ImGuiKey.L,
        [Key.M] = ImGuiKey.M,
        [Key.N] = ImGuiKey.N,
        [Key.O] = ImGuiKey.O,
        [Key.P] = ImGuiKey.P,
        [Key.Q] = ImGuiKey.Q,
        [Key.R] = ImGuiKey.R,
        [Key.S] = ImGuiKey.S,
        [Key.T] = ImGuiKey.T,
        [Key.U] = ImGuiKey.U,
        [Key.V] = ImGuiKey.V,
        [Key.W] = ImGuiKey.W,
        [Key.X] = ImGuiKey.X,
        [Key.Y] = ImGuiKey.Y,
        [Key.Z] = ImGuiKey.Z,
    };

    private bool _controlDown;
    private bool _shiftDown;
    private bool _altDown;
    private bool _superDown;

    private byte[] _iniPathBytes;

    delegate string GetClipboardTextFn(IntPtr userData);

    delegate int SetClipboardTextFn(IntPtr userData, string text);

#pragma warning disable CS8618
    public ImGuiDriver(GraphicsDevice gd)
#pragma warning restore CS8618
    {
        _gd = gd;
        _lastTicks = DateTime.Now.Ticks;

        var context = ImGui.CreateContext();
        ImGui.SetCurrentContext(context);
        var io = ImGui.GetIO();
        io.Fonts.AddFontDefault();
        io.BackendFlags |= ImGuiBackendFlags.RendererHasVtxOffset;
        var factory = gd.ResourceFactory;

        unsafe
        {
            var iniPath = Path.Join(Config.GetDirectory(), "imgui.ini");
            _iniPathBytes = Encoding.ASCII.GetBytes(iniPath);
            fixed (byte* ptr = _iniPathBytes)
                io.NativePtr->IniFilename = ptr;
        }

        // Use SDL clipboard
        io.GetClipboardTextFn = Marshal.GetFunctionPointerForDelegate((GetClipboardTextFn)(_ =>
            Sdl2Native.SDL_GetClipboardText()));
        io.SetClipboardTextFn = Marshal.GetFunctionPointerForDelegate((SetClipboardTextFn)((_, text) =>
            Sdl2Native.SDL_SetClipboardText(text)));
        
        // Create buffers
        RecreateVertexBuffer(16384);
        RecreateIndexBuffer(2048);
        _projMatrixBuffer = factory.CreateBuffer(new BufferDescription(64, BufferUsage.UniformBuffer | BufferUsage.Dynamic));
        _projMatrixBuffer.Name = "ImGui.NET Projection Buffer";

        // Load shaders
        var vertexShaderBytes = LoadShaderCode("imgui-vertex");
        _vertexShader = factory.CreateShader(new ShaderDescription(ShaderStages.Vertex, vertexShaderBytes, "main"));
        var fragmentShaderBytes = LoadShaderCode("imgui-frag");
        _fragmentShader = factory.CreateShader(new ShaderDescription(ShaderStages.Fragment, fragmentShaderBytes, "main"));
        
        // Create layouts
        _layout = factory.CreateResourceLayout(new ResourceLayoutDescription(
            new ResourceLayoutElementDescription("ProjectionMatrixBuffer", ResourceKind.UniformBuffer, ShaderStages.Vertex),
            new ResourceLayoutElementDescription("MainSampler", ResourceKind.Sampler, ShaderStages.Fragment)));
        _textureLayout = factory.CreateResourceLayout(new ResourceLayoutDescription(
            new ResourceLayoutElementDescription("MainTexture", ResourceKind.TextureReadOnly, ShaderStages.Fragment)));
        
        var vertexLayouts = new VertexLayoutDescription[]
        {
            new (
                new VertexElementDescription("in_position", VertexElementSemantic.Position, VertexElementFormat.Float2),
                new VertexElementDescription("in_texCoord", VertexElementSemantic.TextureCoordinate, VertexElementFormat.Float2),
                new VertexElementDescription("in_color", VertexElementSemantic.Color, VertexElementFormat.Byte4_Norm))
        };
        _pipeline = factory.CreateGraphicsPipeline(new GraphicsPipelineDescription(
            BlendStateDescription.SingleAlphaBlend,
            new DepthStencilStateDescription(false, false, ComparisonKind.Always),
            new RasterizerStateDescription(FaceCullMode.None, PolygonFillMode.Solid, FrontFace.Clockwise, false, true),
            PrimitiveTopology.TriangleList,
            new ShaderSetDescription(vertexLayouts, new[] { _vertexShader, _fragmentShader }),
            new[] { _layout, _textureLayout },
            _gd.MainSwapchain.Framebuffer.OutputDescription, 
            ResourceBindingModel.Default));

        // Create font texture
        unsafe // Aw yes, going into the dangerous territory, baby!
        {
            var rbPtr = new ImFontGlyphRangesBuilderPtr(ImGuiNative.ImFontGlyphRangesBuilder_ImFontGlyphRangesBuilder());
            rbPtr.AddRanges(io.Fonts.GetGlyphRangesDefault());
            rbPtr.AddText("áéíýóúůÁÉÍÝÓÚŮžščřďťňěŽŠČŘĎŤŇĚ");
            rbPtr.BuildRanges(out var ranges);

            var fontBytes = GetEmbeddedResourceBytes("LiberationSans-Regular.ttf");
            fixed (byte* ptr = fontBytes)
                MainFont = io.Fonts.AddFontFromMemoryTTF(new IntPtr(ptr), fontBytes.Length, 18f, null, ranges.Data);

            fontBytes = GetEmbeddedResourceBytes("LiberationMono-Regular.ttf");
            fixed (byte* ptr = fontBytes)
                MonospaceFont = io.Fonts.AddFontFromMemoryTTF(new IntPtr(ptr), fontBytes.Length, 18f, null, ranges.Data);
        }
        io.Fonts.AddFontDefault(MainFont.ConfigData);
        io.Fonts.GetTexDataAsRGBA32(out IntPtr pixels, out int width, out int height, out int bytesPerPixel);
        io.Fonts.SetTexID(_fontAtlasId);

        _fontTexture = factory.CreateTexture(TextureDescription.Texture2D(
            (uint)width, (uint)height, 1, 1, PixelFormat.R8_G8_B8_A8_UNorm, TextureUsage.Sampled));
        _fontTexture.Name = "ImGui.NET Font Texture";
        _gd.UpdateTexture(
            _fontTexture, pixels, (uint)(bytesPerPixel * width * height), 0, 0, 0, (uint)width, (uint)height,
            1, 0, 0);
        _fontTextureView = factory.CreateTextureView(_fontTexture);

        io.Fonts.ClearTexData();
        
        // Create resource sets
        _mainResourceSet = factory.CreateResourceSet(new ResourceSetDescription(_layout, _projMatrixBuffer, gd.PointSampler));
        _fontTextureResourceSet = factory.CreateResourceSet(new ResourceSetDescription(_textureLayout, _fontTextureView));
    }

    private void RecreateVertexBuffer(uint size)
    {
        // ReSharper disable once ConditionIsAlwaysTrueOrFalseAccordingToNullableAPIContract
        if (_vertexBuffer != null)
            _gd.DisposeWhenIdle(_vertexBuffer);
        _vertexBuffer = _gd.ResourceFactory.CreateBuffer(new BufferDescription(size,
            BufferUsage.VertexBuffer | BufferUsage.Dynamic));
        _vertexBuffer.Name = "ImGui.NET Vertex Buffer";
    }

    private void RecreateIndexBuffer(uint size)
    {
        // ReSharper disable once ConditionIsAlwaysTrueOrFalseAccordingToNullableAPIContract
        if (_indexBuffer != null)
            _gd.DisposeWhenIdle(_indexBuffer);
        _indexBuffer = _gd.ResourceFactory.CreateBuffer(new BufferDescription(size,
            BufferUsage.IndexBuffer | BufferUsage.Dynamic));
        _indexBuffer.Name = "ImGui.NET Index Buffer";
    }

    public void WindowResized(int width, int height)
    {
        _windowWidth = width;
        _windowHeight = height;
    }

    public void UpdateInput(InputSnapshot input)
    {
        if (_frameBegun)
            ImGui.Render();

        var io = ImGui.GetIO();
        io.DisplaySize = new Vector2(
            _windowWidth, _windowHeight);

        // Delta time
        var nowTicks = DateTime.Now.Ticks;
        var delta = nowTicks - _lastTicks;
        _lastTicks = nowTicks;
        io.DeltaTime = delta / (float) TimeSpan.TicksPerSecond;

        // Mouse
        var mousePosition = input.MousePosition;
        bool leftPressed = false;
        bool middlePressed = false;
        bool rightPressed = false;
        foreach (var me in input.MouseEvents)
        {
            if (!me.Down) continue;
            switch(me.MouseButton)
            {
                case MouseButton.Left:
                    leftPressed = true;
                    break;
                case MouseButton.Middle:
                    middlePressed = true;
                    break;
                case MouseButton.Right:
                    rightPressed = true;
                    break;
            }
        }

        io.MouseDown[0] = leftPressed || input.IsMouseDown(MouseButton.Left);
        io.MouseDown[1] = rightPressed || input.IsMouseDown(MouseButton.Right);
        io.MouseDown[2] = middlePressed || input.IsMouseDown(MouseButton.Middle);
        io.MousePos = mousePosition;
        io.MouseWheel = input.WheelDelta;
        
        // Keyboard
        var keyCharPresses = input.KeyCharPresses;
        foreach (var c in keyCharPresses)
            io.AddInputCharacter(c);

        var keyEvents = input.KeyEvents;
        foreach (var keyEvent in keyEvents)
        {
            if (ImGuiKeys.TryGetValue(keyEvent.Key, out var k))
                io.AddKeyEvent(k, keyEvent.Down);

            switch (keyEvent.Key)
            {
                case Key.ControlLeft or Key.ControlRight:
                    _controlDown = keyEvent.Down;
                    break;
                case Key.ShiftLeft or Key.ShiftRight:
                    _shiftDown = keyEvent.Down;
                    break;
                case Key.AltLeft or Key.AltRight:
                    _altDown = keyEvent.Down;
                    break;
                case Key.WinLeft or Key.WinRight:
                    _superDown = keyEvent.Down;
                    break;
            }
        }

        io.KeyCtrl = _controlDown;
        io.KeyAlt = _altDown;
        io.KeyShift = _shiftDown;
        io.KeySuper = _superDown;
                    
        // New frame
        _frameBegun = true;
        ImGui.NewFrame();
    }

    public void Render(CommandList cl)
    {
        if (!_frameBegun) return;

        _frameBegun = false;
        ImGui.Render();

        var drawData = ImGui.GetDrawData();
        var io = ImGui.GetIO();

        if (drawData.CmdListsCount == 0) 
            return;
        
        uint totalVbSize = (uint)(drawData.TotalVtxCount * Unsafe.SizeOf<ImDrawVert>());
        if (totalVbSize > _vertexBuffer.SizeInBytes)
            RecreateVertexBuffer(totalVbSize * 2);

        uint totalIbSize = (uint)(drawData.TotalIdxCount * sizeof(ushort));
        if (totalIbSize > _indexBuffer.SizeInBytes)
            RecreateIndexBuffer(totalIbSize * 2);
        
        
        uint vertexOffsetInVertices = 0;
        uint indexOffsetInElements = 0;

        for (int i = 0; i < drawData.CmdListsCount; i++)
        {
            var cmdList = drawData.CmdListsRange[i];
            
            cl.UpdateBuffer(
                _vertexBuffer,
                vertexOffsetInVertices * (uint)Unsafe.SizeOf<ImDrawVert>(),
                cmdList.VtxBuffer.Data,
                (uint)(cmdList.VtxBuffer.Size * Unsafe.SizeOf<ImDrawVert>()));
            
            cl.UpdateBuffer(
                _indexBuffer,
                indexOffsetInElements * sizeof(ushort),
                cmdList.IdxBuffer.Data,
                (uint)(cmdList.IdxBuffer.Size * sizeof(ushort)));

            vertexOffsetInVertices += (uint)cmdList.VtxBuffer.Size;
            indexOffsetInElements += (uint)cmdList.IdxBuffer.Size;
        }

        float top, bottom;
        if (_gd.IsClipSpaceYInverted)
        {
            top = io.DisplaySize.Y;
            bottom = 0.0f;
        }
        else
        {
            top = 0.0f;
            bottom = io.DisplaySize.Y;
        }
        Matrix4x4 mvp = Matrix4x4.CreateOrthographicOffCenter(0f, io.DisplaySize.X, bottom, top, -1.0f, 1.0f);
        _gd.UpdateBuffer(_projMatrixBuffer, 0, ref mvp);
        
        cl.SetVertexBuffer(0, _vertexBuffer);
        cl.SetIndexBuffer(_indexBuffer, IndexFormat.UInt16);
        cl.SetPipeline(_pipeline);
        cl.SetGraphicsResourceSet(0, _mainResourceSet);
        
        drawData.ScaleClipRects(io.DisplayFramebufferScale);

        int vtxOffset = 0;
        int idxOffset = 0;
        for (int i = 0; i < drawData.CmdListsCount; i++)
        {
            var cmdList = drawData.CmdListsRange[i];
            for (int j = 0; j < cmdList.CmdBuffer.Size; j++)
            {
                var cmd = cmdList.CmdBuffer[j];
                if (cmd.UserCallback != IntPtr.Zero)
                    throw new NotSupportedException();

                if (cmd.TextureId == _fontAtlasId)
                    cl.SetGraphicsResourceSet(1, _fontTextureResourceSet);
                
                cl.SetScissorRect(
                    0, 
                    (uint)cmd.ClipRect.X, (uint)cmd.ClipRect.Y, 
                    (uint)(cmd.ClipRect.Z - cmd.ClipRect.X), (uint)(cmd.ClipRect.W - cmd.ClipRect.Y));
                
                cl.DrawIndexed(cmd.ElemCount, 1, cmd.IdxOffset + (uint)idxOffset, (int)cmd.VtxOffset + vtxOffset, 0);
            }

            vtxOffset += cmdList.VtxBuffer.Size;
            idxOffset += cmdList.IdxBuffer.Size;
        }
    }
    
    private byte[] LoadShaderCode(string name) => _gd.BackendType switch
    {
        GraphicsBackend.Direct3D11 => GetEmbeddedResourceBytes($"{name}.hlsl.bytes"),
        GraphicsBackend.OpenGL => GetEmbeddedResourceBytes($"{name}.glsl"),
        GraphicsBackend.Vulkan => GetEmbeddedResourceBytes($"{name}.spv"),
        _ => throw new NotSupportedException($"Unsupported backend {_gd.BackendType}")
    };

    private static byte[] GetEmbeddedResourceBytes(string name)
    {
        var assembly = typeof(ImGuiDriver).Assembly;
        using var s = assembly.GetManifestResourceStream(name);
        if (s == null)
            throw new FileNotFoundException($"Resource '{name}' does not exist");
            
        var ret = new byte[s.Length];
        var read = s.Read(ret, 0, (int)s.Length);
        Debug.Assert(read == s.Length);
        return ret;
    }
    
    public void Dispose()
    {
        _fontTextureResourceSet.Dispose();
        _mainResourceSet.Dispose();
        _pipeline.Dispose();
        _textureLayout.Dispose();
        _layout.Dispose();
        _fragmentShader.Dispose();
        _vertexShader.Dispose();
        _fontTextureView.Dispose();
        _fontTexture.Dispose();
        _projMatrixBuffer.Dispose();     
        _indexBuffer.Dispose();
        _vertexBuffer.Dispose();
    }
}
