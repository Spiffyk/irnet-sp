namespace Irnet.Ui.Frontend.DearImGui;

public enum ImportType
{
    IrnetTextFile,
    JavaSerializedFile,
}