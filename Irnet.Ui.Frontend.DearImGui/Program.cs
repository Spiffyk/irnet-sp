﻿using System.Collections.Immutable;
using System.Diagnostics;
using System.Numerics;
using ImGuiNET;
using Irnet.JavaImporter;
using Irnet.Parsers;
using Irnet.Ui.Db;
using Irnet.Utils;
using Veldrid;
using Veldrid.Sdl2;
using Veldrid.StartupUtilities;
using Index = Irnet.Core.Index;

namespace Irnet.Ui.Frontend.DearImGui;

public static class Program
{
    private static readonly AtomicCounter TaskCounter = new();
    private static string _currentOperationDescription = string.Empty;
    private static double _lastOpTime = double.NaN;
    
    private static string _javaImportTooltip = string.Empty;
    private static bool _javaImportAvailable = TrecLoader.IsAvailable(out _javaImportTooltip);
    private static ImportType _importType = ImportType.IrnetTextFile;
    private static bool _fileDialogOpen = false;
    private static string _inputFilePath = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
    
    private static bool _showDemoWindow = false;

    private static string _editorId = string.Empty;
    private static string _editorContent = string.Empty;
    private static bool _editorCreate;
    private static bool _showEditor = false;
    
    private static string _searchQuery = string.Empty;
    private static IImmutableList<string> _vectorQueryHistory = ImmutableList<string>.Empty;
    private static IImmutableList<string> _booleanQueryHistory = ImmutableList<string>.Empty;
    private static int _selectedHistoryEntry = -1;

    private static string _messageWindowTitle = string.Empty;
    private static string _messageWindowText = string.Empty;
    private static bool _showMessageWindow = false;

    private static string _confirmationWindowText = string.Empty;
    private static string _confirmationButtonText = string.Empty;
    private static Action? _confirmationAction = null;
    private static bool _showConfirmationWindow = false;

    private static bool _searched = false;
    private static IImmutableList<RatedDocument> _documents = ImmutableList<RatedDocument>.Empty;

    private static SearchType _searchType = SearchType.VectorSpace;

    /// <summary>
    /// Shows a window with the specified message.
    /// </summary>
    private static void ShowMessage(string text, string title = "Message")
    {
        _messageWindowText = text;
        _messageWindowTitle = title;
        _showMessageWindow = true;
    }
    
    /// <summary>
    /// Shows a confirmation window that performs a cancellable action.
    /// </summary>
    private static void ConfirmedAction(string text, string button, Action action)
    {
        _confirmationWindowText = text;
        _confirmationButtonText = button;
        _confirmationAction = action;
        _showConfirmationWindow = true;
    }

    private static void CreateNewDocument()
    {
        _editorId = string.Empty;
        _editorContent = string.Empty;
        _editorCreate = true;
        _showEditor = true;
    }

    private static void EditDocument(string id, string currentContent)
    {
        _editorId = id;
        _editorContent = currentContent;
        _editorCreate = false;
        _showEditor = true;
    }

    private static void ResetDocuments()
    {
        Interlocked.Exchange(ref _documents, ImmutableList<RatedDocument>.Empty);
        Volatile.Write(ref _searched, false);
    }

    /// <summary>
    /// Executes the specified action, measuring the time to completion. Upon completion, updates the last operation
    /// time stat.
    /// </summary>
    private static void MeasuredOperation(Action action)
    {
        Stopwatch sw = Stopwatch.StartNew();
        try
        {
            action.Invoke();
        }
        finally
        {
            Interlocked.Exchange(ref _lastOpTime, sw.ElapsedMilliseconds * 0.001);
        }
    }

    /// <summary>
    /// Renders the main application menu.
    /// </summary>
    private static void SubmitMenuUi(Sdl2Window sdlWindow)
    {
        if (ImGui.BeginMenuBar())
        {
            if (ImGui.BeginMenu("Irnet"))
            {
                if (ImGui.MenuItem("Create document..."))
                    CreateNewDocument();
                
                ImGui.Separator();
                
                if (ImGui.MenuItem("Load IRNET text file..."))
                    OpenFileDialog(ImportType.IrnetTextFile);

                SubmitJavaImportMenuItem();
                
                ImGui.Separator();
                
                if (ImGui.MenuItem("Clear index..."))
                    ConfirmedAction("Do you really wish to clear the index?", "Yes, clear the index!",
                        () => MeasuredOperation(Core.Index.Clear));
                    
                ImGui.Separator();
                
                if (ImGui.MenuItem("Exit"))
                    sdlWindow.Close();
                

                ImGui.EndMenu();
            }
            
            if (ImGui.BeginMenu("Debug"))
            {
                ImGui.MenuItem("Demo window", null, ref _showDemoWindow);

                ImGui.EndMenu();
            }

            ImGui.EndMenuBar();
        }
    }

    private static void SubmitJavaImportMenuItem()
    {
        if (!_javaImportAvailable)
            ImGui.BeginDisabled();
        
        if (ImGui.MenuItem("Load TREC file..."))
            OpenFileDialog(ImportType.JavaSerializedFile);

        if (!_javaImportAvailable)
        {
            var rectMin = ImGui.GetItemRectMin();
            var rectMax = ImGui.GetItemRectMax();
            ImGui.EndDisabled();
            if (ImGui.IsMouseHoveringRect(rectMin, rectMax))
                ImGui.SetTooltip(_javaImportTooltip);
        }
    }

    private static void OpenFileDialog(ImportType type)
    {
        _importType = type;
        _fileDialogOpen = true;
    }
    
    /// <summary>
    /// Renders the file dialog for loading of indexed data.
    /// </summary>
    private static void SubmitDataLoaderFormUi()
    {
        if (CustomElements.FileDialog("Select input file", ref _fileDialogOpen, ref _inputFilePath))
        {
            string filePath = _inputFilePath;
            _currentOperationDescription = "Loading and indexing file...";
            TaskCounter.Increment();
            Task.Run(() => MeasuredOperation(() => 
            {
                switch (_importType)
                {
                    case ImportType.IrnetTextFile:
                        LoadIrnetTextFile(filePath);
                        break;
                    case ImportType.JavaSerializedFile:
                        LoadJavaSerializedFile(filePath);
                        break;
                }
                TaskCounter.Decrement();
            }));
        }
    }

    private static void LoadIrnetTextFile(string filePath)
    {
        try
        {
            int count = CrawlFileParser.Parse(filePath, Core.Index.AddDocument);
            if (count == 0)
                ShowMessage("No documents found in file");
        }
        catch (Exception e)
        {
            ShowMessage(e.Message, "Error processing file");
        }
    }

    private static void LoadJavaSerializedFile(string filePath)
    {
        var result = TrecLoader.LoadFile(filePath, Core.Index.AddDocument);
        if (!result.Success)
            ShowMessage(result.Reason, "Error processing file");
    }

    /// <summary>
    /// Renders the stats of the <see cref="Core.Index"/>.
    /// </summary>
    private static void SubmitStatusUi(ImGuiDriver drv)
    {
        if (TaskCounter.HasTasks())
        {
            CustomElements.Spinner(drv);
            ImGui.SameLine();
            ImGui.Text(_currentOperationDescription);
            return;
        }

        ImGui.Text($"Document count: {Core.Index.DocumentCount}");
        ImGui.Text($"Unique terms: {Core.Index.TermCount}");
        ImGui.Text($"Last operation time: {Thread.VolatileRead(ref _lastOpTime):N4} seconds");
    }

    private static bool SubmitQueryHistoryUi()
    {
        bool searchSubmit = false;
        if (ImGui.TreeNode("Query history"))
        {
            if (ImGui.BeginListBox("## history", 
                    new Vector2(ImGui.GetContentRegionAvail().X, 5 * ImGui.GetTextLineHeightWithSpacing())))
            {
                var list = _searchType == SearchType.BooleanExpression
                    ? Volatile.Read(ref _booleanQueryHistory)
                    : Volatile.Read(ref _vectorQueryHistory);

                for (int i = 0; i < list.Count; i++)
                {
                    bool selected = i == _selectedHistoryEntry;
                    if (ImGui.Selectable(list[i], selected, ImGuiSelectableFlags.AllowDoubleClick))
                    {
                        _selectedHistoryEntry = i;
                        _searchQuery = list[i];

                        if (ImGui.IsMouseDoubleClicked(ImGuiMouseButton.Left))
                            searchSubmit = true;
                    }
                    
                    if (selected)
                        ImGui.SetItemDefaultFocus();
                }
                
                ImGui.EndListBox();
            }
            ImGui.TreePop();
        }

        return searchSubmit;
    }
    
    /// <summary>
    /// Renders the query controls - the text input and submit button.
    /// </summary>
    private static void SubmitQueryControlsUi()
    {
        ImGui.Text("Search type:");
        ImGui.SameLine();
        if (ImGui.RadioButton("Vector space", _searchType == SearchType.VectorSpace))
            _searchType = SearchType.VectorSpace;
        ImGui.SameLine();
        if (ImGui.RadioButton("TF-IDF", _searchType == SearchType.TfIdf))
            _searchType = SearchType.TfIdf;
        ImGui.SameLine();
        if (ImGui.RadioButton("Boolean expression", _searchType == SearchType.BooleanExpression))
            _searchType = SearchType.BooleanExpression;
        
        ImGui.Text("Query:");
        ImGui.InputText("##Query", ref _searchQuery, 65536);
        bool searchSubmit = ImGui.IsItemFocused() && ImGui.IsKeyPressed(ImGuiKey.Enter);
        
        ImGui.SameLine();
        searchSubmit |= ImGui.Button("Search");
        searchSubmit |= SubmitQueryHistoryUi();

        if (searchSubmit)
        {
            _selectedHistoryEntry = 0;
            DoSearch();
        }
    }

    private static void DoSearch()
    {
        switch (_searchType)
        {
            case SearchType.TfIdf:
                _currentOperationDescription = "Searching and sorting...";
                QueryHistory.PutVector(_searchQuery);
                TaskCounter.Increment();
                Task.Run(() => MeasuredOperation(() =>
                {
                    var query = Core.Index.CreateVectorQuery(_searchQuery);
                    Interlocked.Exchange(ref _documents,
                        Core.Index.FindDocumentsByVectorQuery(query)
                            .Select(did =>
                                new RatedDocument(did.Item1, did.Item2,
                                    Core.Index.CalculateTfIdfRelevance(did.Item2, query)))
                            .OrderByDescending(d => d.Relevance)
                            .ToImmutableList());
                    Volatile.Write(ref _searched, true);

                    TaskCounter.Decrement();
                }));
                break;
            case SearchType.VectorSpace:
                _currentOperationDescription = "Searching and sorting...";
                QueryHistory.PutVector(_searchQuery);
                TaskCounter.Increment();
                Task.Run(() => MeasuredOperation(() =>
                {
                    var query = Core.Index.CreateVectorQuery(_searchQuery);
                    Interlocked.Exchange(ref _documents,
                        Core.Index.FindDocumentsByVectorQuery(query)
                            .Select(did =>
                                new RatedDocument(did.Item1, did.Item2,
                                    Core.Index.CalculateCosineSimilarity(did.Item2, query)))
                            .OrderByDescending(d => d.Relevance)
                            .ToImmutableList());
                    Volatile.Write(ref _searched, true);

                    TaskCounter.Decrement();
                }));
                break;
            case SearchType.BooleanExpression:
                _currentOperationDescription = "Searching...";
                QueryHistory.PutBoolean(_searchQuery);
                TaskCounter.Increment();
                Task.Run(() => MeasuredOperation(() =>
                {
                    var parseResult = Core.Index.CreateBooleanQuery(_searchQuery);
                    if (parseResult.Success)
                    {
                        var query = parseResult.Query;
                        Interlocked.Exchange(ref _documents,
                            Core.Index.FindDocumentsByBooleanQuery(query)
                                .Select(did => new RatedDocument(did.Item1, did.Item2, null))
                                .OrderBy(d => d.Title)
                                .ToImmutableList());
                        Volatile.Write(ref _searched, true);
                    }
                    else
                        ShowMessage(parseResult.Reason, "Query error");

                    TaskCounter.Decrement();
                }));
                break;
        }
        
        UpdateQueryHistory();
    }

    private static void UpdateQueryHistory()
    {
        const int numEntries = 10;
        TaskCounter.Increment();
        Task.Run(() =>
        {
            Interlocked.Exchange(ref _vectorQueryHistory, QueryHistory.GetNewestVector(numEntries).ToImmutableList());
            Interlocked.Exchange(ref _booleanQueryHistory, QueryHistory.GetNewestBoolean(numEntries).ToImmutableList());
            TaskCounter.Decrement();
        });
    }

    /// <summary>
    /// Renders the query results.
    /// </summary>
    private static void SubmitQueryResultsUi()
    {
        var region = ImGui.GetContentRegionAvail();
        region.Y -= 3 * ImGui.GetTextLineHeightWithSpacing();

        if (ImGui.BeginChild("Child", region, true, ImGuiWindowFlags.AlwaysVerticalScrollbar))
        {
            try
            {
                if (!Volatile.Read(ref _searched))
                {
                    ImGui.Text("Please provide a search query using the text input above.");
                    return;
                }

                var docs = Volatile.Read(ref _documents);
                if (docs.Count == 0)
                {
                    ImGui.Text("No documents found.");
                    return;
                }
                
                ImGui.Text($"Found {docs.Count} documents:");

                foreach (var doc in docs)
                {
                    if (!ImGui.TreeNode(doc.Title))
                        continue;
                    if (ImGui.Button("Edit"))
                        EditDocument(doc.Id, doc.Doc.Content);
                    ImGui.SameLine();
                    if (ImGui.Button("Delete"))
                        ConfirmedAction($"Do you really wish to delete document {doc.Id}?", "Yes, delete!",
                            () =>
                            {
                                Index.RemoveDocument(doc.Id);
                                ResetDocuments();
                            });
                    foreach (var line in doc.ContentLines)
                    {
                        ImGui.SetNextItemWidth(ImGui.GetContentRegionAvail().X);
                        ImGui.TextWrapped(line);
                    }
                    ImGui.TreePop();
                }
            }
            finally
            {
                ImGui.EndChild();
            }
        }
    }

    /// <summary>
    /// Renders the message window and the confirmation window.
    /// </summary>
    private static void SubmitMessageWindowUi()
    {
        // Message window
        if (_showMessageWindow && ImGui.Begin(_messageWindowTitle, ref _showMessageWindow,
                ImGuiWindowFlags.NoResize | ImGuiWindowFlags.AlwaysAutoResize))
        {
            if (ImGui.BeginChild("##message", new Vector2(500, 250), false))
            {
                ImGui.TextWrapped(_messageWindowText);
                ImGui.EndChild();
            }

            if (ImGui.Button("Close"))
                _showMessageWindow = false;
            ImGui.End();
        }

        // Confirmation window
        if (_showConfirmationWindow && ImGui.Begin("Confirm", ref _showConfirmationWindow,
                ImGuiWindowFlags.NoResize | ImGuiWindowFlags.AlwaysAutoResize))
        {
            ImGui.Text(_confirmationWindowText);
            if (ImGui.Button("Cancel"))
                _showConfirmationWindow = false;
            ImGui.SameLine();
            if (ImGui.Button(_confirmationButtonText))
            {
                _showConfirmationWindow = false;
                _confirmationAction?.Invoke();
            }

            ImGui.End();
        }
    }

    /// <summary>
    /// Renders the document editor.
    /// </summary>
    public static void SubmitEditorUi()
    {
        if (_showEditor && ImGui.Begin("Document editor", ref _showEditor))
        {
            ImGui.Text("Document ID: ");
            ImGui.SameLine();
            if (_editorCreate)
                ImGui.InputText("## doc ID", ref _editorId, 256);
            else
                ImGui.Text(_editorId);

            var region = ImGui.GetContentRegionAvail();
            region.Y -= 2 * ImGui.GetTextLineHeightWithSpacing();
            ImGui.InputTextMultiline("Content", ref _editorContent, 262144, region);

            if (ImGui.Button("Cancel"))
                _showEditor = false;
            ImGui.SameLine();
            if (ImGui.Button("Save document"))
            {
                _showEditor = false;
                DoSaveEdit();
            }
            
            ImGui.End();
        }
    }

    /// <summary>
    /// Saves the edited document.
    /// </summary>
    private static void DoSaveEdit()
    {
        if (string.IsNullOrWhiteSpace(_editorId))
        {
            ShowMessage("The document ID must not be empty!");
            return;
        }

        if (string.IsNullOrWhiteSpace(_editorContent))
        {
            ShowMessage("The document content must not be empty!");
            return;
        }
        
        var id = _editorId.Trim();
        var content = _editorContent.Trim();

        if (Index.ContainsId(id) && _editorCreate)
        {
            ShowMessage($"A document with ID '{id}' already exists", "Error");
            return;
        }
        
        MeasuredOperation(() =>
        {
            Index.AddDocument(id, content);
            ResetDocuments();
        });
    }
    
    /// <summary>
    /// Renders the main window.
    /// </summary>
    private static void SubmitUi(Sdl2Window sdlWindow, ImGuiDriver drv)
    {
        ImGui.PushFont(drv.MainFont);
        if (ImGui.Begin("Main Window",
                ImGuiWindowFlags.NoCollapse | ImGuiWindowFlags.NoBringToFrontOnFocus | ImGuiWindowFlags.NoResize |
                ImGuiWindowFlags.NoMove | ImGuiWindowFlags.MenuBar | ImGuiWindowFlags.NoDecoration))
        {
            ImGui.SetWindowPos(Vector2.Zero);
            ImGui.SetWindowSize(new Vector2(sdlWindow.Width, sdlWindow.Height));

            bool disabled = IsGuiDisabled();
            if (disabled) 
                ImGui.BeginDisabled();
            
            SubmitMenuUi(sdlWindow);
            SubmitQueryControlsUi();
            SubmitQueryResultsUi();
            
            if (disabled) 
                ImGui.EndDisabled();
            
            SubmitStatusUi(drv);
            SubmitDataLoaderFormUi();
            SubmitMessageWindowUi();
            SubmitEditorUi();
            
            if (_showDemoWindow)
                ImGui.ShowDemoWindow(ref _showDemoWindow);
            
            ImGui.End();
        }

        ImGui.PopFont();
    }

    /// <summary>
    /// Updates relevant sizes based on the specified SDL2 window.
    /// </summary>
    private static void UpdateSizes(Sdl2Window window, ImGuiDriver drv, GraphicsDevice gd)
    {
        gd.MainSwapchain.Resize((uint)window.Width, (uint)window.Height);
        drv.WindowResized(window.Width, window.Height);
    }

    private static bool IsGuiDisabled() => TaskCounter.HasTasks() || _fileDialogOpen || _showEditor;

    public static void Main()
    {
        Config.EnsureDirectoryCreated();
        UiDb.Touch();
        UpdateQueryHistory();
        
        VeldridStartup.CreateWindowAndGraphicsDevice(
            new WindowCreateInfo(100, 100, 1024, 720, WindowState.Normal, "Hello, world!"),
            new GraphicsDeviceOptions(true, null, true, ResourceBindingModel.Improved),
            out var window,
            out var gd);
        
        //using (gd) // FIXME: hangs on gd.Dispose() for some reason
        using (var drv = new ImGuiDriver(gd))
        using (var cl = gd.ResourceFactory.CreateCommandList())
        {
            // ReSharper disable once AccessToDisposedClosure
            window.Resized += () => UpdateSizes(window, drv, gd);
            UpdateSizes(window, drv, gd);

            while (window.Exists)
            {
                var input = window.PumpEvents();
                if (!window.Exists)
                    break;
                drv.UpdateInput(input);

                SubmitUi(window, drv);
                if (!window.Exists)
                    break;

                cl.Begin();
                cl.SetFramebuffer(gd.MainSwapchain.Framebuffer);
                cl.ClearColorTarget(0, new RgbaFloat(0.3f, 0.0f, 0.3f, 1.0f));
                drv.Render(cl);
                cl.End();
                gd.SubmitCommands(cl);
                gd.SwapBuffers(gd.MainSwapchain);
            }

            gd.WaitForIdle();
        }
    }
}
