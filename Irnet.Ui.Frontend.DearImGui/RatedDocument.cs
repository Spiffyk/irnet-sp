using System.Collections.Immutable;
using System.Text;
using Irnet.Core;

namespace Irnet.Ui.Frontend.DearImGui;

public record RatedDocument(string Id, Document Doc, double? Relevance)
{
    // Optimization so that we don't re-generate the title on each window render.
    private readonly Lazy<string> _title = new(() =>
    {
        var sb = new StringBuilder();
        sb.Append($"{Id}");
        if (Relevance != null)
            sb.Append($" (relevance: {Relevance:N4})");
        return sb.ToString();
    });

    // We need to split and escape the content because Dear ImGui interprets format strings and the C# bindings only
    // expose the `fmt` and not he variadic parameters.
    private readonly Lazy<IImmutableList<string>> _contentLines = new(() => Doc.Content.Split('\n')
        .Select(l => l.Trim())
        .Where(l => !string.IsNullOrWhiteSpace(l))
        .Select(l => l.Replace("%", "%%"))
        .ToImmutableArray());

    public string Title => _title.Value;
    public IImmutableList<string> ContentLines => _contentLines.Value;
}