namespace Irnet.Ui.Frontend.DearImGui;

public enum SearchType
{
    TfIdf,
    VectorSpace,
    BooleanExpression
}