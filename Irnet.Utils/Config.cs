﻿namespace Irnet.Utils;

public static class Config
{
    private const string DirectoryName = "irnet-sp";
    
    /// <returns>Configuration directory.</returns>
    public static string GetDirectory()
    {
        string appData = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
        return Path.Join(appData, DirectoryName);
    }

    /// <summary>
    /// Makes sure that the configuration directory has been created.
    /// </summary>
    public static void EnsureDirectoryCreated()
    {
        var d = GetDirectory();
        if (Directory.Exists(d))
            return;
        Directory.CreateDirectory(d);
    }
}